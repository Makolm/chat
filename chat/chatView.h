﻿
// chatView.h: интерфейс класса CchatView
//

#pragma once

#include "server_socket.h"

class CchatView : public CScrollView
{
protected: // создать только из сериализации
	CchatView() noexcept;
	DECLARE_DYNCREATE(CchatView)

// Атрибуты
public:
	CchatDoc* GetDocument() const;

	server_socket server;
	client_socket client;
	
// Операции
public:

// Переопределение
public:
	virtual void OnDraw(CDC* pDC);  // переопределено для отрисовки этого представления
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // вызывается в первый раз после конструктора
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Реализация
public:
	virtual ~CchatView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Созданные функции схемы сообщений
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // версия отладки в chatView.cpp
inline CchatDoc* CchatView::GetDocument() const
   { return reinterpret_cast<CchatDoc*>(m_pDocument); }
#endif

