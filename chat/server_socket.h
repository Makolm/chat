#pragma once

class server_socket : CSocket{
	CList<SOCKET, SOCKET> clients;
public:

	void server_start();
	void add_client(SOCKET s) { clients.AddTail(s); }

	void OnAccept(int);

};

class client_socket : CSocket {
public:
	void connect_to_server();
};